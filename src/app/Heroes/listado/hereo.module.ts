import { HeroeComponent } from "../Heroe/Heroe.component";
import { ListadoComponent } from "./listado.component";
import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
@NgModule({
    declarations:[
        HeroeComponent,
        ListadoComponent
    ],
    exports : [
        ListadoComponent,
        HeroeComponent
    ], 
    imports: [
        CommonModule
    ]

})

export class HereoeeModule{

}
