import { Component } from '@angular/core';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html'
})
export class ListadoComponent {

  heroes: string [] =["Hulk", "Spiderman", "Capitan America", "IronMan", "Thor", "Doctor Strange"]
  heroeBorrado: string

  borrarHeroe(){
    this.heroeBorrado = this.heroes.shift() || ''
    console.log(this.heroes)
  }
  sizeHeroes(): boolean{
    if(this.heroes.length == 0 )return false
    return true
  }



}
