import { Component } from '@angular/core';

@Component({
    selector: 'app-heroe',
    templateUrl:'./Heroe.component.html'

})
export class HeroeComponent{
    name : String = 'IronMan'
    edad : number = 45
    
    getName() : String{
        return this.name        
    }

    obtenerNombre() : String{
        return `${this.name} - ${this.edad}`
    }
    cambiarNombre(): void{
        this.name = 'Spiderman'
    }
    cambiarEdad():void{
        this.edad = 30
    }

}