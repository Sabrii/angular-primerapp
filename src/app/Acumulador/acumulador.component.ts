import { Component } from '@angular/core';
@Component({
    selector: "app-acumulador",

    template: `<p class="parrafo">{{title}} </p>
    <button (click)="sumar()" >+1</button>
    <span>{{numero}}</span>
    <button (click)="restar()"  >-1</button>
    
    <button (click)="acumulador(base)">{{base}}</button>
    <span>{{numero2}}</span>`, 
    styleUrls: ['../app.component.css']

})
export class ContadorComponent {
    title = 'Pagina con Angular';
    base: number = 5
    numero = 1
    numero2: number = 0
    sumar() {
        this.numero += 1
    }
    restar() {
        this.numero -= 1
    }

    acumulador(valor: number) {
        this.numero2 += valor
    }

}