import { NgModule } from '@angular/core';

import { ContadorComponent } from './acumulador.component';


@NgModule({
    declarations: [
        ContadorComponent        
    ],
    exports: [
        ContadorComponent
    ],

})

export class AcumuladorModule{

}