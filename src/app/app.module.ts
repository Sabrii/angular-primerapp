import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ContadorComponent } from "./Acumulador/acumulador.component";
import { HereoeeModule } from './Heroes/listado/hereo.module';
import { AcumuladorModule } from './Acumulador/acumulador.module';
import { DbzModule } from './dbz/dbz.module';

@NgModule({
  declarations: [
    AppComponent,


  ],
  imports: [
    BrowserModule,
    HereoeeModule, 
    AcumuladorModule,
    DbzModule

    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
