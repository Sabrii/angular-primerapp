import { Injectable } from "@angular/core";
import { Personaje } from '../Interface/personajes.interface';


@Injectable()
export class DbzService{
    constructor(){
        console.log("servicio inicializado ")
    }
    private _personajes :Personaje[] =[{
        nombre: "Goku",
        poder: 15000
      },
      {
        nombre: "Vegueta",
        poder: 14500
      }]

     
      get getPersonaje():Personaje[]{
          return [...this._personajes]
      }
     
      agregar(personaje: Personaje){
        this._personajes.push(personaje)
      }

}