import { Component , Input} from '@angular/core';
import { Personaje } from '../Interface/personajes.interface';
import { DbzService } from "../Service/dbz.service";

@Component({
  selector: 'app-personajes',
  templateUrl: './personajes.component.html',
  styleUrls: ['./personajes.component.css']
})
export class PersonajesComponent {
  constructor(private dbzService: DbzService){

  }
  // @Input()

  //   personajes : Personaje [] =[]

    get personajes (){
      return this.dbzService.getPersonaje
    }

}
